
// Dependencies
import Dependencies._

/**/

// ThisBuild
//ThisBuild / scalaVersion     := "2.11.12"
//ThisBuild / scalaVersion     := "2.12.0"
//ThisBuild / scalaVersion     := "2.13.1"
//ThisBuild / scalaVersion     := "2.13.3"
//ThisBuild / scalaVersion     := "2.13.5"
//ThisBuild / crossScalaVersions := Seq("2.11.12", "2.13.5")
//ThisBuild / scalaVersion     := "3.0.2"
//ThisBuild / scalaVersion     := "3.1.3"
//ThisBuild / scalaVersion     := "3.2.0"
ThisBuild / scalaVersion     := "3.3.3"
//ThisBuild / scalaVersion     := "3.4.2"
//ThisBuild / version          := "0.2.1"
//ThisBuild / version          := "0.3.0"
ThisBuild / version          := "1.7.6"
ThisBuild / organization     := "org.sanzo"
ThisBuild / organizationName := "sanzo"


ThisBuild / scalacOptions ++= Seq(
	"-unchecked", "-deprecation", "-feature"
    	)
//scalacOptions in (Compile, doc) ++= Seq("-groups", "-implicits")

ThisBuild / scalacOptions ++= {
  CrossVersion.partialVersion(scalaVersion.value) match {
    case Some((3, minor)) if minor <= 3 =>
	//Seq("-source:3.0-migration", "-Xignore-scala2-macros", "-explain", "-explain-types", "-noindent", "-old-syntax" )
	//Seq("-source:3.0-migration", "-Xignore-scala2-macros", "-explain", "-explain-types")
	//Seq("-source:future-migration", "-Xignore-scala2-macros", "-explain", "-explain-types", "-noindent", "-old-syntax" )
	//Seq("-source:future-migration", "-Xignore-scala2-macros", "-explain", "-explain-types" )
	Seq("-source:future", "-Xignore-scala2-macros", "-explain", "-explain-types" )
    case _ =>
	Seq()
  }
}


ThisBuild / resolvers += "Maven Releases" at "https://repo1.maven.org/maven2/"
ThisBuild / resolvers += "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
ThisBuild / resolvers += "JBoss 3rd-party" at "https://repository.jboss.org/nexus/content/repositories/thirdparty-releases/"


ThisBuild / autoAPIMappings := true
/*
ThisBuild / apiURL := Some(url("https://www.sanzo.org/doc/scaladoc/scaladoc_2.11/org.sanzo/"))

ThisBuild / apiMappings += (
        (unmanagedBase.value / "breeze_2.13-1.2.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/breeze_2.13-1.2/")
    )
ThisBuild / apiMappings += (
        (unmanagedBase.value / "biojava.jar") ->
        url("https://biojava.org/docs/api/")
    )

ThisBuild / apiMappings += (
        (unmanagedBase.value / "org-sanzo-biojava.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/org.sanzo/org-sanzo-biojava/")
    )
ThisBuild / apiMappings += (
        (unmanagedBase.value / "org-sanzo-breeze.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/org.sanzo/org-sanzo-breeze/")
    )
ThisBuild / apiMappings += (
        (unmanagedBase.value / "org-sanzo-gsl.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/org.sanzo/org-sanzo-gsl/")
    )
ThisBuild / apiMappings += (
        (unmanagedBase.value / "org-sanzo-molevol.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/org.sanzo/org-sanzo-molevol/")
    )
ThisBuild / apiMappings += (
        (unmanagedBase.value / "org-sanzo-potts.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/org.sanzo/org-sanzo-potts/")
    )
ThisBuild / apiMappings += (
        (unmanagedBase.value / "org-sanzo-sequence.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/org.sanzo/org-sanzo-sequence/")
    )
ThisBuild / apiMappings += (
        (unmanagedBase.value / "org-sanzo-structure.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/org.sanzo/org-sanzo-structure/")
    )
ThisBuild / apiMappings += (
        (unmanagedBase.value / "org-sanzo-utility.jar") ->
        url("https://www.sanzo.org/doc/scaladoc/org.sanzo/org-sanzo-utility/")
    )
*/

// projects
/**/
libraryDependencies ++= {
  CrossVersion.partialVersion(scalaVersion.value) match {
    case Some((2, major)) if major < 13 =>
      Seq()
    case Some((2, major)) if major >= 13 =>
      Seq("org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4")
    case Some((3, minor)) if minor >=  0 =>
      Seq("org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4")
    case _ => ???
  }
}

/* moved into project/Dependencies.scala
lazy val scalaCheck = "org.scalacheck" %% "scalacheck" % "1.14.0"
lazy val scalaCtic  = "org.scalactic" %% "scalactic" % "3.0.8"
//lazy val scalaTest  = "org.scalatest" %% "scalatest" % "3.0.8"
*/

lazy val root = (project in file("."))
  .settings(
    name := "org-sanzo-potts",
  //libraryDependencies += scalaCollectionCompat	,	// for scala 2.11, 2.12
  //libraryDependencies += toolkit      ,
  //libraryDependencies += toolkitTest  ,
  //libraryDependencies += spire	,
    libraryDependencies += breeze	,
    libraryDependencies += breezeNatives,
    libraryDependencies += breezeViz	,
    libraryDependencies += netlibJava	,
    libraryDependencies += netlibNativeRef ,
    libraryDependencies += netlibNativeSystem ,
    libraryDependencies += biojavaCore	,

    libraryDependencies += scalaTest % Test
  )

// Uncomment the following for publishing to Sonatype.
// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for more detail.

// ThisBuild / description := "Some descripiton about your project."
// ThisBuild / licenses    := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))
// ThisBuild / homepage    := Some(url("https://github.com/example/project"))
// ThisBuild / scmInfo := Some(
//   ScmInfo(
//     url("https://github.com/your-account/your-project"),
//     "scm:git@github.com:your-account/your-project.git"
//   )
// )
// ThisBuild / developers := List(
//   Developer(
//     id    = "Your identifier",
//     name  = "Your Name",
//     email = "your@email",
//     url   = url("http://your.url")
//   )
// )
// ThisBuild / pomIncludeRepository := { _ => false }
// ThisBuild / publishTo := {
//   val nexus = "https://oss.sonatype.org/"
//   if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
//   else Some("releases" at nexus + "service/local/staging/deploy/maven2")
// }
// ThisBuild / publishMavenStyle := true

assembly / assemblyMergeStrategy := {
  case PathList("javax", "servlet", xs @ _*)         => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".xml" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".types" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".class" => MergeStrategy.first
  case "application.conf"                            => MergeStrategy.concat
  case "unwanted.txt"                                => MergeStrategy.discard
  case x =>
    val oldStrategy = (assembly / assemblyMergeStrategy).value
    oldStrategy(x)
}

